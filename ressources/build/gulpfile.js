// Include gulp
var gulp = require('gulp');

// Include Plugins
//var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');

var postcss = require('gulp-postcss');
var autoprefixer = require('autoprefixer');
var cssnano = require('cssnano');

var size = require('gulp-size');
var clean = require('gulp-clean');

//Plugins pour optimiser les JS
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');

var browserSync = require('browser-sync').create();
var reload = browserSync.reload;

// Récupération des paramètres de configuration pour Gulp
var config = require('./gulp.conf.js');



// ---------------------------------------------------------------------------
// COPY
// ---------------------------------------------------------------------------

// Copie les assets nécessaires aux composants bower
gulp.task('copyFiles', function () {
    // Copie les fonts de bootstrap    
    gulp.src( config.PATHS.BOWER_COMPONENTS + 'bootstrap/fonts/*.*' )
        .pipe( gulp.dest( config.PATHS.FONTS+ '/bootstrap' ) )
});


// ---------------------------------------------------------------------------
// CSS
// ---------------------------------------------------------------------------

gulp.task('styles-dev', function() {

    return gulp
        .src(config.FILES_VENDOR_CSS)
        .pipe(size({title: 'CSS >',showFiles: true, showTotal: false}))
        .pipe(concat('application.css'))
        //.pipe(sourcemaps.init)
        //.pipe(sourcemaps.write('../../public/css'))
        .pipe(postcss([ autoprefixer({ browsers: config.AUTOPREFIXER_BROWSERS }) ]))
        .pipe(size({title: 'BUILD STYLES ---> application.css'}))
        .pipe(gulp.dest(config.PATHS.CSS_PROD))
        .pipe(browserSync.stream({match: '**/*.css'}));
});

gulp.task('styles-prod', function() {
    return gulp
        .src(config.FILES_VENDOR_CSS)
        .pipe(size({title: 'CSS >', showFiles: true, showTotal: false}))
        // Avec minification CSS
        .pipe(postcss([ autoprefixer({ browsers: config.AUTOPREFIXER_BROWSERS }), cssnano({zindex:false}) ]))
        .pipe(concat('application.css'))
        .pipe(size({title: 'BUILD STYLES ---> application.css (Minimized)'}))
        .pipe(gulp.dest(config.PATHS.CSS_PROD));
});

// ---------------------------------------------------------------------------
// JS
// ---------------------------------------------------------------------------

gulp.task('js-vendor-dev', function() { // >> public/js/application.js
    return gulp
        .src(config.FILES_VENDOR_JS)
        .pipe(size({title: 'JS >',showFiles: true, showTotal: false}))
        .pipe(concat('application.js'))
        .pipe(size({title: 'BUILD JS ---> application.js'}))
        .pipe(gulp.dest(config.PATHS.JS_ROOT));
});


gulp.task('js-vendor-prod', function() { // >> public/js/application.js (Minified)
    return gulp
        .src(config.FILES_VENDOR_JS)
        .pipe(size({title: 'JS >',showFiles: true, showTotal: false}))
        .pipe(uglify())
        .pipe(concat('application.js'))
        .pipe(size({title: 'BUILD JS ---> application.js (Minimized)'}))
        .pipe(gulp.dest(config.PATHS.JS_ROOT));
});

// Surveillance des modifications de fichiers
gulp.task('watch', function () {
    
    // Build du style de l'application
    gulp.watch(config.PATHS.CSS_SRC + '**/**/*.scss', ['styles-dev']);

    // Build du javascript de l'application
    // gulp.watch( config.PATHS.JS_APP + '**/*.js').on('change', reload );

    // Build du PHP
    gulp.watch( config.PATHS.PHP_SRC + '**/*').on('change', reload );
});

// Lancement du BrowserSync
gulp.task('launchBrowserSync', function () {
    // Paramétres de lancement de BrowserSync
    browserSync.init({
        // lance le navigateur configuré par defaut, sinon on peut spécifier un navigateur en particulier
        //browser: ["firefox"],
        browser: ["chrome"],
        //browser: ["firefox","chrome"],
        proxy: config.URL_DEV,
        // open: false, // Empeche l'ouverture d'un onglet à chaque redémarrage
        // reloadOnRestart: true, // Reload à chaque redémarrage
    });
});


// --------------------------------------------------------------------------
// TACHES
// --------------------------------------------------------------------------

// Crée les js et css pour que tout fonctionne
gulp.task('default', ['styles-dev', 'js-vendor-dev', 'copyFiles']);

// Créé les js, css et surveille
gulp.task('dev', ['styles-dev', 'js-vendor-dev', 'copyFiles', 'launchBrowserSync', 'watch']);

// Créé les fichiers nécessaires pour la prod
gulp.task('prod', ['styles-prod', 'js-vendor', 'copyFiles']);
