@author David Messager
@version 0.0.1

## Environnement de développement

```shell
# Définir HTTPS pour Git : 
git config --system url."https://".insteadOf git://

# Si nécessaire, le paramétrage d proxy se fait comme ceci
# dans git
git config --system http.proxy http://proxy.host:8080
git config --system https.proxy http://proxy.host:8080
# en variables d'environnement
export HTTP_PROXY=http://proxy.host:8080
export HTTPS_PROXY=http://proxy.host:8080
```

* Vérifier la présence de NPM, le gestionnaire de package de node : 
`npm --version`
* Installer Bower de manière globale avec : 
`npm install -g bower`
* Installer Gulp de manière globale : 
`npm install -g gulp`
`npm install -g gulp-cli`

## Après un checkout du projet

* Récupération des modules nodes dans `/build/node_modules`
```shell
npm install
``` 

* Récupération des modules bower dans le répertoire `/build/bower_components`
```shell
bower install
```

* Pendant la phase de développement :
 - `gulp` pour créer les fichiers javascript et css nécessaire au fonctionnement de l'application (compilation des fichiers en one-shot)
 - `gulp dev` pour lancer un navigateur auto-synchronisé avec le code (php, css, js) qui se met à jour en temps réel

* Pendant la génération du package de production :
 - `gulp prod` permet de générer les css et js nécessaire pour la production : minifié

## Commandes secondaires :
**npm start** : racourci de gulp dev

**npm list** : liste les packages node utilisés dans le projet (sans montrer les dépendances)

**npm listg** : liste les packages node installés de manière global (sans montrer les dépendances)