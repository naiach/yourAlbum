// Initialisation des chemins utilisés dans GULP
var PATHS = new function () {
    this.BUILD 			    = "./"; 								                    // dossier de travail du build
    this.ROOT 				= "../../"; 							                    // dossier racine du projet
    this.BOWER_COMPONENTS 	= this.BUILD               + 'bower_components/'; 	        // dossier des composants bower
    this.SASS_ROOT          = this.BUILD               + 'sass/'; 				        // dossier source du style SASS
    this.CSS_SRC            = this.BUILD               + 'sass/app/'; 			        // dossier source du style SASS
    this.PHP_SRC			= this.ROOT                + 'module/'; 				    // dossier source du PHP
    this.PROD				= this.ROOT                + 'public/'; 				    // dossier de prod
    this.JS_ROOT			= this.PROD                + 'js/'; 					    // dossier racine du JS
    this.JS_APP             = this.PROD                + 'js/app/'; 					// dossier racine de l'application JS
    this.JS_VENDOR			= this.PROD                + 'js/vendor/'; 					// dossier contenant les ressources du VENDOR
    this.IMG				= this.PROD                + 'img/'; 			            // dossier de prod des images
    this.FONTS				= this.PROD                + 'fonts/'; 				        // dossier de prod contenant les polices de caractères
    this.CSS_PROD           = this.PROD                + 'css/'; 				        // dossier de prod CSS
};

PACKAGE = require(PATHS.BUILD + 'package.json');

module.exports = {
    // Initialise les chemins
    PATHS : PATHS,

    // Package.json
    PACKAGE : PACKAGE,

    // Url pour BrowserSync
    URL_DEV : "localhost/" + PACKAGE.name,

    // Bandeau en entête de fichiers CSS et JS
    BANNER : [
        '/**',
        ' ** <%= PACKAGE.name %> - <%= PACKAGE.description %>',
        ' ** @author <%= PACKAGE.author %>',
        ' ** @version v<%= PACKAGE.version %>',
        ' **/',
        ''
    ].join('\n'),

    // Paramétrage par default de bootstrap 3
    AUTOPREFIXER_BROWSERS : [
        "Android 2.3",
        "Android >= 4",
        "Chrome >= 20",
        "Firefox >= 24",
        "Explorer >= 8",
        "iOS >= 6",
        "Opera >= 12",
        "Safari >= 6"
    ],

    // Liste des fichiers CSS
    FILES_VENDOR_CSS : [
        PATHS.BOWER_COMPONENTS + 'bootstrap/dist/css/bootstrap.min.css',
    ],

    // Liste des fichiers JS utilisés pour obtenir le fichier JS VENDOR FRONT
    FILES_VENDOR_JS : [
        PATHS.BOWER_COMPONENTS + 'jquery/dist/jquery.js',
        PATHS.BOWER_COMPONENTS + 'bootstrap/dist/js/bootstrap.js'       
    ],

}
