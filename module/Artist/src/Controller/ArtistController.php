<?php
/**
 * Created by PhpStorm.
 * User: MSI
 * Date: 09/10/2017
 * Time: 14:12
 */

namespace Artist\Controller;
use Artist\Model\Artist;
use Artist\Form\ArtistForm;
use Artist\Model\ArtistTable;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class ArtistController extends AbstractActionController
{

    // Add this property:
    private $table;

    // Add this constructor:
    public function __construct(ArtistTable $table)
    {
        $this->table = $table;
    }


    public function indexAction()
    {
        return new ViewModel([
            'artist' => $this->table->fetchAll(),
        ]);
    }

    public function addAction()
    {
        $form = new ArtistForm();
        $form->get('submit')->setValue('Add');

        $request = $this->getRequest();

        if (! $request->isPost()) {
            return ['form' => $form];
        }

        $artist = new Artist();
        $form->setInputFilter($artist->getInputFilter());
        $form->setData($request->getPost());

        if (! $form->isValid()) {
            return ['form' => $form];
        }

        $artist->exchangeArray($form->getData());
        $this->table->saveArtist($artist);
        return $this->redirect()->toRoute('artist');
    }

    public function editAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);

        if (0 === $id) {
            return $this->redirect()->toRoute('artist', ['action' => 'add']);
        }

        // Retrieve the artist with the specified id. Doing so raises
        // an exception if the artist is not found, which should result
        // in redirecting to the landing page.
        try {
            $artist = $this->table->getArtist($id);
        } catch (\Exception $e) {
            return $this->redirect()->toRoute('artist', ['action' => 'index']);
        }

        $form = new ArtistForm();
        $form->bind($artist);
        $form->get('submit')->setAttribute('value', 'Edit');

        $request = $this->getRequest();
        $viewData = ['id' => $id, 'form' => $form];

        if (! $request->isPost()) {
            return $viewData;
        }

        $form->setInputFilter($artist->getInputFilter());
        $form->setData($request->getPost());

        if (! $form->isValid()) {
            return $viewData;
        }

        $this->table->saveArtist($artist);

        // Redirect to artist list
        return $this->redirect()->toRoute('artist', ['action' => 'index']);
    }

    public function deleteAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('artist');
        }

        $request = $this->getRequest();
        if ($request->isPost()) {
            $del = $request->getPost('del', 'No');

            if ($del == 'Yes') {
                $id = (int) $request->getPost('id');
                $this->table->deleteArtist($id);
            }

            // Redirect to list of artists
            return $this->redirect()->toRoute('artist');
        }

        return [
            'id'    => $id,
            'artist' => $this->table->getArtist($id),
        ];
    }
}