<?php

namespace Artist\Form;

use Zend\Form\Form;

class ArtistForm extends Form
{
    public function __construct($name = null)
    {
        // We will ignore the name provided to the constructor
        parent::__construct('artist');

        $this->add([
            'name' => 'id',
            'type' => 'hidden',
        ]);

        $this->add([
            'name' => 'firstname',
            'type' => 'text',
            'options' => ['label' => 'Firstname'],

        ]);

        $this->add([
            'name' => 'lastname',
            'type' => 'text',
            'options' => ['label' => 'Lastname'],
        ]);

        $this->add([
            'name' => 'age',
            'type' => 'text',
            'options' => ['label' => 'Age'],
        ]);

        $this->add([
            'name' => 'genre',
            'type' => 'text',
            'options' => ['label' => 'Genre'],
        ]);

        $this->add([
            'name' => 'submit',
            'type' => 'submit',
            'attributes' =>  ['value' => 'Go',
                'id'    => 'submitbutton',],
        ]);
    }
}