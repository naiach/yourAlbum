<?php

namespace Artist\Model;

use DomainException;
use Zend\Filter\StringTrim;
use Zend\Filter\StripTags;
use Zend\Filter\ToInt;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\Validator\StringLength;

class Artist implements InputFilterAwareInterface
{
	public $id;
	public $firstname;
	public $lastname;
	public $age;
	public $genre;
	
	public function exchangeArray(array $data)
	{
		$this->id		= !empty($data['id']) ? $data['id'] : null;
		$this->firstname	= !empty($data['firstname']) ? $data['firstname'] : null;
		$this->lastname	= !empty($data['lastname']) ? $data['lastname'] : null;
        $this->age	= !empty($data['age']) ? $data['age'] : null;
		$this->genre	= !empty($data['genre']) ? $data['genre'] : null;
	}
	
	
	public function getArrayCopy()
	{
		return [
				'id'     => $this->id,
				'firstname' => $this->firstname,
				'lastname'  => $this->lastname,
                'age'  => $this->age,
                'genre' => $this->genre
		];
	}
	/* Add the following methods: */
	
	public function setInputFilter(InputFilterInterface $inputFilter)
	{
		throw new DomainException(sprintf('%s does not allow injection of an alternate input filter',__CLASS__));
	}
	
	public function getInputFilter()
	{
		if ($this->inputFilter) {
			return $this->inputFilter;
		}
		
		$inputFilter = new InputFilter();
		
		$inputFilter->add([
				'name' => 'id',
				'required' => true,
				'filters' => [['name' => ToInt::class],],
		]);
		
		$inputFilter->add([
				'name' => 'firstname',
				'required' => true,
				'filters' => [
						['name' => StripTags::class],
						['name' => StringTrim::class],
				],
				'validators' => [
						[
								'name' => StringLength::class,
								'options' => [
										'encoding' => 'UTF-8',
										'min' => 1,
										'max' => 100,],
						],
				],
		]);

        $inputFilter->add([
            'name' => 'lastname',
            'required' => true,
            'filters' => [
                ['name' => StripTags::class],
                ['name' => StringTrim::class],
            ],
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 1,
                        'max' => 100,],
                ],
            ],
        ]);
		
		$inputFilter->add([
				'name' => 'genre',
				'required' => true,
				'filters' => [
						['name' => StripTags::class],
						['name' => StringTrim::class],
				],
				'validators' => [
						[
								'name' => StringLength::class,
								'options' => [
										'encoding' => 'UTF-8',
										'min' => 1,
										'max' => 100,
								],
						],
				],
		]);

        $inputFilter->add([
            'name' => 'age',
            'required' => true,
            'filters' => [['name' => ToInt::class],],
        ]);
		
		$this->inputFilter = $inputFilter;
		return $this->inputFilter;
	}
}
	
