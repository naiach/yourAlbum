<?php
/**
 * Created by PhpStorm.
 * User: MSI
 * Date: 09/10/2017
 * Time: 14:06
 */

namespace Artist;

use Zend\ServiceManager\Factory\InvokableFactory;
use Zend\Router\Http\Segment;

return [

    // The following section is new and should be added to your file:
    'router' => [
        'routes' => [
            'artist' => [
                'type'    => Segment::class,
                'options' => [
                    'route' => '/artist[/:action[/:id]]',
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                    ],
                    'defaults' => [
                        'controller' => Controller\ArtistController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
        ],
    ],

    'view_manager' => [
        'template_path_stack' => [
            'artist' => __DIR__ . '/../view',
        ],
    ],
];